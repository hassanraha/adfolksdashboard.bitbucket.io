
let planetChartData = {
    type: 'horizontalBar',
    data: {
        labels: ['January'],
        datasets: [{
            label: 'Dataset 2',
            backgroundColor: "#04a773",
            data: [
                20,
                10
            ]
        }, {
            label: 'Dataset 3',
            backgroundColor: "#9c332f",
            data: [
                10,
                20
            ]
        }]

    },
    options: {
        legend: {
            display: false
        },
        tooltips: {
            mode: 'index',
            intersect: false
        },
        responsive: true,
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    display: false
                },
                gridLines: {
                    drawBorder: false,
                    display: false
                }
            }],
            yAxes: [{
                ticks: {
                    display: false
                },
                gridLines: {
                    drawBorder: false,
                    display: false
                },
                stacked: true
            }]
        }
    }
}

export default planetChartData;